﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public interface DocumentObject
{
	Sprite Preview { get; }
	string Name { get; }
}
