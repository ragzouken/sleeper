﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class DocumentSound : DocumentObject
{
	public static Sprite DefaultPreview = Resources.Load<Sprite>("default-sound-preview");
	
	public Sprite Preview { get; private set; }
	public string Name { get; set; }
	
	public DocumentSound()
	{
		Name = "unnamed sound";
		Preview = DefaultPreview;
	}
}
