﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class DocumentImage : DocumentObject
{
	public static Sprite DefaultPreview = Resources.Load<Sprite>("default-image-preview");

	public Sprite Preview { get; private set; }
	public string Name { get; set; }

	public DocumentImage()
	{
		Name = "unnamed image";
		Preview = DefaultPreview;
	}
}
