﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ObjectPreview : MonoBehaviour
{
    [SerializeField] protected Image Image;
    [SerializeField] protected Text	Name;

	public DocumentObject Object
	{
		set 
		{
			Image.sprite = value.Preview;
			Name.text = value.Name;
		}
	}
}
