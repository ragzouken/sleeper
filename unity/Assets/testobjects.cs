﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class testobjects : MonoBehaviour
{
	public Transform container;
	public ObjectPreview prefab;
	public Sprite prev;

	public void Awake()
	{
		//DocumentImage.DefaultPreview = prev;

		var preview = Instantiate(prefab) as ObjectPreview;
		preview.transform.SetParent(container, false);

		preview.Object = new DocumentImage();

		preview = Instantiate(prefab) as ObjectPreview;
		preview.transform.SetParent(container, false);
		
		preview.Object = new DocumentSound();
	}
}
